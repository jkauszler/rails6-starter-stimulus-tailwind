# Rails 6 Starter

A simple Rails 6 starting point. Using Webpacker, Stimulus, TailwindCSS (with PurgeCSS to reduce file size).

## Setup

* Ruby version
`Ruby 2.6.5`

* System dependencies

* Configuration
To modify env variables or credentials use:

```bash
EDITOR=nano rails credentials:edit
```

From there you can access the credentials with `Rails.application.credentials.oauth[:oauth_token]` or `Rails.application.credentials.<credential-parent>[:<credential-name>]`.

For more information on this newer Rails procedure see:
https://blog.saeloun.com/2019/10/10/rails-6-adds-support-for-multi-environment-credentials.html

* Database creation

```bash
rails db:create
```

* Database initialization

```bash
rails db:migrate
```

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
